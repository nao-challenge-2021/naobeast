---
title: About us
subtitle: Conosciamoci un po'
comments: false
---

Ciao a tutti! Siamo il gruppo NAObeast di Bolzano.
Le caratteristiche del nostro gruppo:

- Determinazione
- Costanza nel lavoro
- Operosità
- Grande coesione del gruppo

### Vuoi conoscerci meglio?

Se vuoi sapere altro su di noi visita il [nostro canale YouTube](https://www.youtube.com/channel/UCBLRH0ncnM5JvVZtSptEcmA) 
e non dimenticare di iscriverti!
