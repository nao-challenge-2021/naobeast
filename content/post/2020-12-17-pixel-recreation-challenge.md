---
title: Pixel Recreation Challenge
date: 2020-12-17
tags: ["NAO", "art"]
---
{{< gviewer 1d_Kg5_50xZPDF55JXxJDf4HZiMDKVQ-Fx3MblJL3OKs >}}

Ciao a tutti! Questo è il blog di NAObeast, siamo una squadra partecipante alla NAO Challenge 2021, ecco la nostra prima sfida:
Ci è stato chiesto di ricreare delle opere d'arte con la pixel-art. Nonostante nessuno di noi abbia mai provato questo tipo di arte ci siamo comunque impegnati al massimo, ed ecco quindi il risultato del nostro lavoro!

P.S.: Di volta in volta caricheremo nuovi aggiornamenti, state allerta per nuovi aggiornamenti!
