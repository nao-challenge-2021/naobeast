---
title: Pensieri personali sulla NAO challenge
date: 2021-03-07
tags: ["NAO", "thoughts"]
bigimg: [{src: "/img/Rainerum.jpeg", desc: "Rainerum"}]
---

Quando siamo entrati a far parte della NAO challenge abbiamo raccolto prime impressioni, propositi e semplici pensieri sulla competizione.

<!--more-->

Niccolò Dametto:

> La NAO è una competizione fantastica che ti permette di crescere.

<!--more-->

Isabella Bertolini:

> Penso che la NAO sia un ottimo spazio dove posso trovarmi con la mia classe e migliorare il gioco di squadra. Tutti hanno un ruolo ben preciso, ma comunque ognuno di noi cerca di aiutare anche gli altri per cercare di affrontare questa competizione con lo spirito giusto.

<!--more-->

Dario Spitaleri:

> Credo che NAO sia l'occasione per imparare a lavorare di squadra.

<!--more-->

Johannes Maximilian Lorenzi:

> Credo che sia molto importante nella vita il lavoro di squadra e che questa oltre che un occasione per divertirsi assieme sia proprio un momento nel quale affinare il nostro lavoro di squadra, dividendosi il lavoro e divertendosi con un obbiettivo comune.

<!--more-->

Ludovica Costa:

> Grazie alla NAO sono riuscita a riscoprire lo spirito di squadra che è stato fondamentale per portare avanti un progetto come questo.

<!--more-->

Riccardo Defrancesco:

> Grazie alla NAO non solo ho migliorato la mia caacità di lavorare in squadra, ma ho anche stretto amicizie che prima d'ora non avrei mai immaginato, NAO è stata quindi per tutti sia un motivo per mettersi in gioco e legare maggiormante con i membri del gruppo.
