---
title: Video prove seconda fase
date: 2021-03-08
tags: ["NAO", "squadra"]
---
Prima prova:
{{< youtube id="9fa2y3tEvis" >}}

Seconda prova:
{{< youtube id="-YXsKLddO7c" >}}

Terza prova:
{{< youtube id="D1ZQmt9LMnM" >}}
